# Roteiro - Monitoramento na AWS - Parte 2 e 3

Saudações pessoal!
No último video do canal, que está disponível [aqui](https://youtu.be/SXsgSGcUOxQ) falamos sobre a criação de uma stack de monitoramento completa utilizando o Prometheus integrado ao Grafana, na AWS.

Hoje vamos continuar montando nosso laboratório!

Este texto está dividido nas seguintes etapas:
- [Roteiro - Monitoramento na AWS - Parte 2 e 3](#roteiro---monitoramento-na-aws---parte-2-e-3)
  - [1. Criação do Ambiente utilizando Docker Compose](#1-criação-do-ambiente-utilizando-docker-compose)
  - [2. Configurando DNS](#2-configurando-dns)
  - [3. Configuração do Prometheus para capturar as métricas geradas por nossa máquina local](#3-configuração-do-prometheus-para-capturar-as-métricas-geradas-por-nossa-máquina-local)
  - [4. Configuração do Grafana para receber os dados capturados pelo prometheus](#4-configuração-do-grafana-para-receber-os-dados-capturados-pelo-prometheus)
  - [5. Verificação de dados capturados após maior tempo de coleta](#5-verificação-de-dados-capturados-após-maior-tempo-de-coleta)
  - [6. Configuração do proxy reverso para acesso via DNS](#6-configuração-do-proxy-reverso-para-acesso-via-dns)

## 1. Criação do Ambiente utilizando Docker Compose

Todo nosso ambiente está configurado em um arquivo Docker Compose.

Docker Compose é uma ferramenta desenvolvida para ajudar a definir e compartilhar aplicativos com vários contêineres. Com o Compose, você pode criar um arquivo YAML para definir os serviços e com um único comando, pode criar ou destruir todo o ambiente.

O Docker Compose é recomendado para laboratórios, como o que estamos fazendo!

> Lembro que o roteiro deste video está disponível no GitLab, e o link está na descrição.

No video anterior acessamos o repositório do projeto, e hoje vamos clonar esse repositório na nossa Instância AWS. Para isso, acesse sua instância, eleve seus privilégios para root, e execute o comando:

```bash
git clone https://gitlab.com/sg-wolfgang/stack-prometheus.git
cd stack-prometheus
docker-compose up -d
```

Mas, o que este arquivo faz? Vamos ver?

```yaml
version: '3'

services:
  reverse:
    container_name: reverse_proxy
    hostname: reverse_proxy
    image: nginx:latest
    ports:
      - 80:80
      - 443:443
    volumes:
      - ./nginx:/etc/nginx
      - ./ssl:/etc/ssl/private
    restart: unless-stopped

  prometheus:
    container_name: prometheus
    hostname: prometheus
    image: prom/prometheus:latest
    ports:
      - 9090:9090
    volumes:
      - ./prometheus:/etc/prometheus
    restart: unless-stopped
    command: --web.enable-lifecycle  --config.file=/etc/prometheus/prometheus.yml
  
  node-exporter:
    container_name: node-exporter
    hostname: node-exporter
    image: prom/node-exporter:latest
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
    command: 
      - '--path.procfs=/host/proc' 
      - '--path.sysfs=/host/sys'
      - --collector.filesystem.ignored-mount-points
      - "^/(sys|proc|dev|host|etc|rootfs/var/lib/docker/containers|rootfs/var/lib/docker/overlay2|rootfs/run/docker/netns|rootfs/var/lib/docker/aufs)($$|/)"
    ports:
      - 9100:9100
    restart: unless-stopped

  docker-exporter:
    container_name: docker-exporter
    hostname: docker-exporter
    image: prometheusnet/docker_exporter:latest
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    ports:
      - 9417:9417
    restart: always

  grafana:
    container_name: grafana
    hostname: grafana
    image: grafana/grafana:latest
    ports:
      - 3031:3000
    volumes:
      - grafana-storage:/var/lib/grafana
    restart: unless-stopped
  
  portainer:
    container_name: portainer
    hostname: portainer
    image: portainer/portainer-ce:latest
    ports:
      - "9000:9000"
      - "8000:8000"
    volumes:
      - portainer_data:/data
      - /var/run/docker.sock:/var/run/docker.sock
    restart: always
  
  letsencrypt:
    image: linuxserver/letsencrypt
    container_name: letsencrypt
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=America/Sao_Paulo
      - URL=example.com
      - SUBDOMAINS=wildcard,
      - VALIDATION=dns
      - DNSPLUGIN=cloudflare
      - EMAIL=email@example.com
      - DHLEVEL=4096
    volumes:
      - ./config:/config
    restart: unless-stopped

volumes:
  portainer_data:
  grafana-storage:
  etc-nginx:
  ssl-nginx:
```

Certo, agora que nosso ambiente está Up, precisaremos configurar cada um dos serviços disponíveis. No video de hoje, vamos aprender a configurar um serviço gratuito de DNS para atrelar ao nosso ambiente. Mas, se você possui um serviço DNS pode atrelar a ele, sem maiores problemas.

Falando em DNS, vocês querem que eu traga videos falando sobre o assunto aqui no canal? Deixa nos comentários!

## 2. Configurando DNS

Vamos acessar o site do freenom e criar um dominio gratuito. [Clique aqui para acessar](https://www.freenom.com/pt/index.html?lang=pt) e registrar seu domínio gratuitamente!

> Você precisará criar uma conta no freenom para utilizar nesta etapa.

## 3. Configuração do Prometheus para capturar as métricas geradas por nossa máquina local


## 4. Configuração do Grafana para receber os dados capturados pelo prometheus

Sugestões de dashboards:
* https://grafana.com/grafana/dashboards/1860?pg=dashboards&plcmt=featured-sub1
* https://grafana.com/grafana/dashboards/11074
* https://grafana.com/grafana/dashboards/11467

## 5. Verificação de dados capturados após maior tempo de coleta
Nesta etapa é importante quie você deixe seu sistema executando por algumas horas, e verifique os consumos! 

## 6. Configuração do proxy reverso para acesso via DNS
Precisaremos editar os sequintes arquivos para finalizar as configurações do proxy reverso:

* Entre no diretório correto, o que você utilizou para fazer o download dos arquivos do repositório com o comando:

```bash
cd /root/stack-prometheus/nginx/conf.d/sites-available
```

* Edite os arquivos realizando as alterações abaixo:

```bash
vim grafana.conf
```
* Edite o arquivo para que fique como:
```bash
upstream grafana {
  server        grafana:3000;
}

server {
  listen        80;
  server_name   grafana.sgoncalves.cf;

#  include       common.conf;
#  include       /etc/nginx/ssl.conf;

  location / {
    proxy_pass  http://grafana;
#    include    common_location.conf;
  }
}
```

> Obs.: Verifique pois seu domínio certamente é diferente do meu, altere-o!

* Abra o arquivo `portainer.conf`

```bash
vim portainer.conf
```

* Edite o arquivo para que fique como:

```bash
upstream portainer {
  server        portainer:9000;
}

server {
  listen        80;
  server_name   portainer.sgoncalves.cf;

#  include       common.conf;
#  include       /etc/nginx/ssl.conf;

  location / {
    proxy_pass  http://portainer;
#    include    common_location.conf;
  }
}
```

> Obs.: Verifique pois seu domínio certamente é diferente do meu, altere-o!

* Abra o arquivo `prometheus.conf`

```bash
vim prometheus.conf
```

* Edite o arquivo para que fique como:

```bash
upstream prometheus {
  server        prometheus:9090;
}

server {
  listen        80;
  server_name   prometheus.sgoncalves.cf;

#  include       common.conf;
#  include       /etc/nginx/ssl.conf;

  location / {
    auth_basic           "Prometheus";
    auth_basic_user_file /etc/nginx/.htpasswd;
    proxy_pass  http://prometheus;
#    include    common_location.conf;
  }
}
```
> Obs.: Verifique pois seu domínio certamente é diferente do meu, altere-o!

* Renomeie o arquivo `redirect.conf`:

```bash
mv redirect.conf redirect.conf.bkp 
```

Agora, reinicie o container `reverse_proxy` utilizando o portainer que configuramos.

Feito isso temos configurado todo o ambiente! Podemos realizar os testes.